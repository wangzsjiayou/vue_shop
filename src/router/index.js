import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/login.vue'
import Home from '../components/home.vue'
import Welcome from '../components/welcome.vue'
import Users from '../components/user/users.vue'
import Rights from '../components/power/rights.vue'
import Roles from '../components/power/roles.vue'
import Cate from '../components/goods/cate.vue'
import Params from '../components/goods/params.vue'
import List from '../components/goods/list.vue'
import AddGood from '../components/goods/addGood.vue'
import Order from '../components/order/order.vue'
import Reports from '../components/reports/report.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/', redirect: '/Login' },
  { path: '/login', component: Login },

  {
    path: '/home',
    component: Home,
    // 如果是/home 就重定向到/welcome页面
    redirect: '/welcome',
    // /home的子路由数组  对象
    children: [
      { path: '/welcome', component: Welcome },
      { path: '/users', component: Users },
      { path: '/rights', component: Rights },
      { path: '/roles', component: Roles },
      { path: '/categories', component: Cate },
      { path: '/params', component: Params },
      { path: '/goods', component: List },
      { path: '/goods/add', component: AddGood },
      { path: '/orders', component: Order },
      { path: '/reports', component: Reports }

    ]
  }

]
// 实例化路由对象
const router = new VueRouter({
  routes
})

// 为路由对象挂载路由导航守卫
router.beforeEach((to, from, next) => {
  // to代表要访问的路径
  // from 代表从哪儿个路径跳转而来
  // next放行函数
  // next()表示放行  next('/login') 表示强制跳转
  if (to.path === '/login') {
    return next()
  }
  const token = window.sessionStorage.getItem('token')
  if (!token) {
    return next('/login')
  }
  next()
})
export default router

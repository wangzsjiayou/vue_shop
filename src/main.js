import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
// 导入全局样式
import './assets/css/global.css'
// 导入字体图标
import './assets/fonts/iconfont.css'
import axios from 'axios'
// 导入第三方依赖 table tree
import TreeTable from 'vue-table-with-tree-grid'
// 导入第三方文本编辑器
import VueQuillEditor from 'vue-quill-editor'
import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme

Vue.use(VueQuillEditor)

// 设置请求根路径
axios.defaults.baseURL = 'http://127.0.0.1:8888/api/private/v1/'
// axios请求拦截
axios.interceptors.request.use(config => {
  // 为请求头设置token
  config.headers.Authorization = window.sessionStorage.getItem('token')
  return config
})
// 把axios 挂在Vue的$http属性上,且所有对象共享数据,原型对象可以看做面向对象的父类
Vue.prototype.$http = axios
Vue.config.productionTip = false
// 将table tree 注册到全局
Vue.component('tree-table', TreeTable)
// 注册全局过滤器        使用场景: date | 过滤器()
// 时间过滤器
Vue.filter('dateFormat', function (originVal) {
  const dt = new Date(originVal * 1000)
  console.log(originVal)
  // 获取年
  const y = dt.getFullYear()
  // 获取月
  const m = (dt.getMonth() + 1 + '').padStart(2, '0')
  // 获取日
  const d = (dt.getDate() + '').padStart(2, '0')
  // 获取时
  const h = (dt.getHours() + '').padStart(2, '0')
  // 获取分
  const i = (dt.getMinutes() + '').padStart(2, '0')
  // 获取秒
  const s = (dt.getSeconds() + '').padStart(2, '0')
  // 返回模板字符串
  return `${y}-${m}-${d} ${h}:${i}:${s}`
})
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
